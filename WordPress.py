from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
import sys
import time
import pdb

class Browser(object):
    def __init__(self, browserType, timeout = 30):
        self.browser = None
        if browserType.lower() == "firefox":
            self.browser = webdriver.Firefox()
        elif browserType.lower() == "chrome":
            self.browser = webdriver.Chrome()
        elif browserType.lower() == "opera":
            self.browser = webdriver.Opera()
        self.browser.set_page_load_timeout(timeout)

    def get_browser(self):
        return self.browser

    def set_url(self, url):
        if url == None or url == "":
            print "A valid url is required"
            return
        self.url = url
        self.browser.get(url)

    def quit_browser(self):
        try:
            self.browser.quit()
        except NoSuchElementException as err:
            print "Element error({0})".format(err.msg)
        except WebDriverException as err:
            print "WebDriver error({0})".format(err.msg)

class WordPress(object):
    def __init__(self, browser):
        self.browser = browser

    def login(self, username, password):
        ele=self.browser.browser.find_element_by_id('user_login')
#       ele.send_keys('admin')
        ele.send_keys(username)
        ele=self.browser.browser.find_element_by_id('user_pass')
#       ele.send_keys('Broad401')
        ele.send_keys(password)
        ele=self.browser.browser.find_element_by_id('wp-submit')
        ele.submit()

    def add_image(self, name):
        pdb.set_trace()
        ele = self.browser.browser.find_element_by_id('wpwrap').find_element_by_id('wpcontent').find_element_by_id('wpbody').find_element_by_id('wpbody-content').find_element_by_class_name('wrap').find_element_by_id('post')
        ele1 = ele.find_element_by_id('post-body').find_element_by_id('postbody-container-1').find_element_by_id('side-sortables').find_element_by_id('postimagediv').find_element_by_class_name('inside').find_element_by_tag_name('a')

    def posts(self):
        ele = self.browser.browser.find_element_by_xpath("id('wpwrap')").find_element_by_xpath("id('adminmenuwrap')").find_element_by_xpath("id('adminmenu')").find_element_by_xpath("id('menu-posts')")
        ele1=ele.find_element_by_link_text('Add New')
        self.browser.set_url(ele1.get_attribute('href'))

    def add_new_post(self, title, text, imagename):
        if len(imagename) > 0:
            self.add_image(imagename)

        ele = self.browser.browser.find_element_by_id('wpwrap').find_element_by_id('wpcontent').find_element_by_id('wpbody').find_element_by_id('wpbody-content').find_element_by_class_name('wrap').find_element_by_id('post')
        # title
        ele1 = ele.find_element_by_id('title')
        ele1.send_keys(title)
        # text body, but select text, instead of visual, first
        ele2 = ele.find_element_by_id('post-body').find_element_by_id('post-body-content').find_element_by_id('postdivrich').find_element_by_id('wp-content-wrap').find_element_by_id('wp-content-editor-tools').find_element_by_id('content-html')
        ele2.click() 
        ele3 = ele.find_element_by_id('post-body').find_element_by_id('post-body-content').find_element_by_id('postdivrich').find_element_by_id('wp-content-wrap').find_element_by_id('wp-content-editor-container').find_element_by_id('content') 
#       pdb.set_trace()
        ele3.send_keys(text)
        # publish
        ele4 = ele.find_element_by_id('post-body').find_element_by_id('postbox-container-1').find_element_by_id('publish') 
        ele4.click()
        time.sleep(15)

    def logout(self):
        ele = self.browser.browser.find_element_by_xpath("id('wpwrap')").find_element_by_xpath("id('wpcontent')").find_element_by_xpath("id('wpadminbar')").find_element_by_xpath("id('wp-admin-bar-my-account')")

        ele1 = ele.find_element_by_id('wp-admin-bar-logout').find_element_by_class_name('ab-item')
        url=ele1.get_attribute('href')
        self.browser.set_url(url)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: sys.argv[0] <url> <username> <password>"
        sys.exit(1)

    # Create a new instance of the Firefox driver
    try:
        browser = Browser("Firefox")
    except Exception as err:
        print "This sxcript uses Firefox as the browser. Make sure you have Firefox installed"
        sys.exit(1)

    url = "http://" + sys.argv[1].strip() + "/wordpress/wp-admin"
#   browser.set_url("http://192.168.1.117/wordpress/wp-admin")
    browser.set_url(url)

    # wordpress instance
    wp = WordPress(browser)
    # login
    wp.login(sys.argv[2].strip(), sys.argv[3].strip())

    imagename = ""
    if len(sys.argv) > 4:
        imagename = sys.argv[4].strip()
    wp.posts()
    wp.add_new_post('a new post', 'hello, world! Really.', imagename)
    time.sleep(2)

    # logout
    wp.logout()

    browser.quit_browser()

