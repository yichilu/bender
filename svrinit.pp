#class wordpresssvr {
  notice($architecture)

  $puppetmodules = "/etc/puppetlabs/puppet/modules"

  # install httpd if not installed
  package { httpd: ensure => installed}

  # run httpd if not running
  service { httpd:
    ensure => running,
  }

# add iptables input rules for http
  $sbin = "/sbin"
  $iptablescmd = "/sbin/iptables "
  $iptableshttp = "-I INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT"
  $execiptableshttp = "${iptablescmd}${iptableshttp}"
  $execiptablessave = "/sbin/service iptables save"
  $execiptablesrestart = "/sbin/service iptables restart"
  notice($execiptableshttp)
  notice($execiptablessave)
  notice($execiptablesrestart)

  exec { $execiptableshttp:
    cwd   => $puppetmodules,
    path  => [$sbin],
    before => Exec[$execiptablessave]
  }

  exec { $execiptablessave:
    cwd   => $puppetmodules,
    path  => [$sbin],
    require => Exec[$execiptableshttp],
    before => Exec[$execiptablesrestart]
  }

  exec { $execiptablesrestart:
    cwd   => $puppetmodules,
    path  => [$sbin],
    require => Exec[$execiptablessave],
  }

# add Servername=hostname
  $bin = "/bin"
  $echocmd = "/bin/echo "
  $httpservername = "ServerName=`hostname` >> "
  $httpdconf = "/etc/httpd/conf/httpd.conf"
  $execechohttpservername = "${echocmd}${httpservername}${httpdconf}"
  notice($execechohttpservername)

  exec { $execechohttpservername:
    cwd   => $puppetmodules,
    path  => [$bin],
  }

  # install wget php php-mysql if not installed
  package { wget: ensure => installed}
  package { php: ensure => installed}
  package { php-mysql: ensure => installed}
  # for running selenium
  package { firefox: ensure => installed}

  exec_cmd_no_require {["ls /tmp/latest.tar.gz"]:
    cwd => "/tmp",
    path => $bin,
  }

# $wget = "/usr/bin/wget "
# $wpsite  = "http://wordpress.org/"
# $execwgetwpinstaller = "${wget}${wpsite}${wpinstaller}"

# exec { $execwgetwpinstaller:
#   cwd   => $tmp,
#   path  => [$bin],
#   before => Exec[$exectar],
# }
# notice($execwgetwpinstaller)
  
  # extract wordpress to /var/www/html/
  $tmp = "/tmp/"
  $wpinstaller = "latest.tar.gz"
  $tar = "/bin/tar -xzf "
  $Cdir = " -C /var/www/html "
  $exectar = "${tar}${tmp}${wpinstaller}${Cdir}"
  notice($exectar)

  exec { $exectar:
    cwd   => "/tmp",
    path  => [$bin],
#   require => File["${tmp}${wpinstaller}"],
  }
  
#}
