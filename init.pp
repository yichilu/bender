#class wordpressdb {
  notice($architecture)

  $puppetmodules = "/etc/puppet/modules"

# add iptables input rules for mysql and puppet
  $sbin = "/sbin"
  $iptablescmd = "/sbin/iptables "
  $iptablesmysql = "-I INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT"
  $iptablespuppettcp= "-I INPUT -m state --state NEW -m tcp -p tcp --dport 8140 -j ACCEPT"
  $iptablespuppetudp= "-I INPUT -m state --state NEW -m udp -p udp --dport 8140 -j ACCEPT"
  $execiptablesmysql = "${iptablescmd}${iptablesmysql}"
  $execiptablespuppettcp = "${iptablescmd}${iptablespuppettcp}"
  $execiptablespuppetudp = "${iptablescmd}${iptablespuppetudp}"
  $execiptablessave = "/sbin/service iptables save"
  $execiptablesrestart = "/sbin/service iptables restart"
  notice($execiptablesmysql)
  notice($execiptablespuppettcp)
  notice($execiptablespuppetudp)

  exec { $execiptablesmysql:
    cwd   => $puppetmodules,
    path  => [$sbin],
  }

  exec { $execiptablespuppettcp:
    cwd   => $puppetmodules,
    path  => [$sbin],
  }

  exec { $execiptablespuppetudp:
    cwd   => $puppetmodules,
    path  => [$sbin],
  }

  exec { $execiptablessave:
    cwd   => $puppetmodules,
    path  => [$sbin],
    require => Exec[$execiptableshttp],
    before => Exec[$execiptablesmysql]
    before => Exec[$execiptablespuppettcp]
    before => Exec[$execiptablespuppetudp]
  }

  exec { $execiptablesrestart:
    cwd   => $puppetmodules,
    path  => [$sbin],
    require => Exec[$execiptablessave],
  }

# create database "wordpress"

  package {'mysql-server':
         ensure => present,
  }

  service {'mysqld':
         require => Package['mysql-server'],
         ensure => running,
  }

  $usrbin = "/usr/bin"
  $mysqlcmd = "/usr/bin/mysql "
  $user  = "--user=root "
  $password = "--password=password "
  $mysql_script="< /etc/puppet/modules/wordpressdb/files/wp.sql > /etc/puppet/modules/output.txt"
  $execmysql = "${mysqlcmd}${user}${password}${mysql_script}"
  notice($execmysql)

  exec { $execmysql:
    cwd   => $puppetmodules,
    path  => [$usrbin],
  }

#}

